﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using DevTerm.Interfaces.Services;

namespace DevTerm.Services
{
    public class DialogService : IDialogService
    {
        public Task ShowDialog(string message, string title, string buttonLabel)
        {
            return UserDialogs.Instance.AlertAsync(message, title, buttonLabel);
        }

        public void ShowToast(string message)
        {
            UserDialogs.Instance.Toast(message);
        }
    }
}
