﻿using System;
using Autofac;
using DevTerm.Interfaces.Services;
using DevTerm.Services;
using DevTerm.ViewModels;

namespace DevTerm.Bootstrap
{
    public class AppContainer
    {
        private static IContainer _container;

        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            //ViewModels
            builder.RegisterType<MainViewModel>().SingleInstance();
            builder.RegisterType<ServerConnectionViewModel>();
            //builder.RegisterType<HomeViewModel>();

            //builder.RegisterType<CheckoutViewModel>();
            //builder.RegisterType<ShoppingCartViewModel>().SingleInstance();

            //services - data
            builder.RegisterType<NavigationService>().As<INavigationService>();
            builder.RegisterType<DialogService>().As<IDialogService>();
            //builder.RegisterType<CatalogDataService>().As<ICatalogDataService>();

            //services - general
            //builder.RegisterType<SettingsService>().As<ISettingsService>().SingleInstance();

            //General

            _container = builder.Build();
        }

        public static void SetupMessengers()
        {
            Resolve<MainViewModel>().InitializeMessenger();
        }

        public static object Resolve(Type typeName)
        {
            var data = _container.Resolve(typeName);
            return data;
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
