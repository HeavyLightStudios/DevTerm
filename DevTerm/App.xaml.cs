﻿using System;
using System.Threading.Tasks;
using DevTerm.Bootstrap;
using DevTerm.Interfaces.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DevTerm
{
    public partial class App : Application
    {
        public App()
        {
            try
            {
                InitializeComponent();

                InitializeApp();

                InitializeNavigation();
            }
            catch (Exception ex)
            {

            }
        }

        private async Task InitializeNavigation()
        {
            var navigationService = AppContainer.Resolve<INavigationService>();
            await navigationService.InitializeAsync();
        }

        private void InitializeApp()
        {
            AppContainer.RegisterDependencies();
            AppContainer.SetupMessengers();
            //var shoppingCartViewModel = AppContainer.Resolve<ShoppingCartViewModel>();
            //shoppingCartViewModel.InitializeMessenger();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
