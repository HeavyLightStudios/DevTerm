﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DevTerm.Interfaces.Services;
using DevTerm.ViewModels.Base;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Xamarin.Forms;

namespace DevTerm.ViewModels
{
    public class MainViewModel : ViewModelBase
    {

        string _location;
        Interfaces.Services.IFilePicker _filePicker;

        public MainViewModel(INavigationService navigationService, IDialogService dialogService) : base(navigationService, dialogService)
        {
        }

        public override void InitializeMessenger()
        {
            base.InitializeMessenger();

        }

        public ICommand PickFiles => new Command(FilePicker);
        public ICommand ConnectToServer => new Command(ConnectServer);
        public ICommand RunServerCommand => new Command(InternalRunServerCommand);

        private void FilePicker()
        {
            _filePicker = DependencyService.Get<Interfaces.Services.IFilePicker>();
            _filePicker.FilePicked += FilePicker_FilePicked;
            _filePicker.FilePicker();
        }

        private async void ConnectServer(object obj)
        {
            await _navigationService.NavigateToAsync<ServerConnectionViewModel>();
        }


        void FilePicker_FilePicked(object sender, Models.FileLocationEventArgs e)
        {
            _location = e.Location;


        }


        void InternalRunServerCommand()
        {
            _filePicker.SendFileAsync(_location);
        }

    }
}
