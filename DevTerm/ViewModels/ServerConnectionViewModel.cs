﻿using System;
using System.Windows.Input;
using DevTerm.Interfaces.Services;
using DevTerm.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.ComponentModel;

namespace DevTerm.ViewModels
{
    public class ServerConnectionViewModel : ViewModelBase
    {

        public ServerConnectionViewModel(INavigationService navigationService, IDialogService dialogService) : base(navigationService, dialogService)
        {
            SetDefaultValues();
        }

        string serverUrl, serverUsername, serverPassword;
        public string ServerUrl
        {
            get
            {
                return serverUrl;
            }
            set
            {
                if (serverUrl != value)
                {
                    serverUrl = value;
                    OnPropertyChanged("ServerUrl");
                }
            }
        }

        public string ServerUsername
        {
            get
            {
                return serverUsername;
            }
            set
            {
                if (serverUsername != value)
                {
                    serverUsername = value;
                    OnPropertyChanged("ServerUsername");
                }
            }
        }

        public string ServerPassword
        {
            get
            {
                return serverPassword;
            }
            set
            {
                if (serverPassword != value)
                {
                    serverPassword = value;
                    OnPropertyChanged("ServerPassword");
                }
            }
        }

        public ICommand SaveServerSettings => new Command(InternalSaveServerSettings);
        public ICommand ClearServerSettings => new Command(InterClearServerSettings);

        private async void InternalSaveServerSettings()
        {
            try
            {
                await SecureStorage.SetAsync("server_url", ServerUrl);
                await SecureStorage.SetAsync("server_username", ServerUsername);
                await SecureStorage.SetAsync("server_password", ServerPassword);
                Console.WriteLine("Made it");
            }
            catch (Exception ex)
            {
                // Possible that device doesn't support secure storage on device.
                Console.Write($"ServerConnectionViewModel: {ex.Message}");
            }


            await _navigationService.NavigateBackAsync();   
            
        }

        void InterClearServerSettings()
        {
            try
            {
                SecureStorage.Remove("server_url");
                SecureStorage.Remove("server_username");
                SecureStorage.Remove("server_password");

                serverUrl = String.Empty;
                serverUsername = String.Empty;
                serverPassword = String.Empty;

                OnPropertyChanged("ServerUrl");
                OnPropertyChanged("ServerUsername");
                OnPropertyChanged("ServerPassword");            
            }
            catch(Exception ex)
            {
                Console.WriteLine($"ServerConnectionViewModel: {ex.Message}");
            }
        }

        private async void SetDefaultValues()
        {
            serverUrl = await SecureStorage.GetAsync("server_url");
            serverUsername = await SecureStorage.GetAsync("server_username");
            serverPassword = await SecureStorage.GetAsync("server_password");
        }
    }
}
