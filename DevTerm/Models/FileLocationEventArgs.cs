﻿using System;
namespace DevTerm.Models
{
    public class FileLocationEventArgs : EventArgs
    {
        public string Location { get; } = "";

        public FileLocationEventArgs(string location)
        {
            Location = location;
        }
    }
}
