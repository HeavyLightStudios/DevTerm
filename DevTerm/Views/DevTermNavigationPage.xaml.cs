﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DevTerm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DevTermNavigationPage : NavigationPage
    {
        public DevTermNavigationPage()
        {
            InitializeComponent();
        }

        public DevTermNavigationPage(Page root) : base(root)
        {
            InitializeComponent();
        }
    }
}
