﻿using System;
using DevTerm.Models;

namespace DevTerm.Interfaces.Services
{
    public delegate void FileLocationHandler(object sender, FileLocationEventArgs e);

    public interface IFilePicker
    {
        event FileLocationHandler FilePicked;
        void FilePicker();
        void SendFileAsync(string path);
    }
}
