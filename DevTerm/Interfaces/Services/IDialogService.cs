﻿using System;
using System.Threading.Tasks;

namespace DevTerm.Interfaces.Services
{
    public interface IDialogService
    {
        Task ShowDialog(string message, string title, string buttonLabel);

        void ShowToast(string message);
    }
}
