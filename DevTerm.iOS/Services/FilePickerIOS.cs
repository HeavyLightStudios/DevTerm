﻿using System;
using System.IO;
using DevTerm.Interfaces.Services;
using DevTerm.iOS.Services;
using DevTerm.Models;
using Foundation;
using Renci.SshNet;
using UIKit;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(FilePickerIOS))]
namespace DevTerm.iOS.Services
{
    public class FilePickerIOS : IFilePicker
    {
        public event FileLocationHandler FilePicked;

        public void FilePicker()
        {
            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;

            while (vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }

            var directoryViewController = new UIDocumentPickerViewController(new string[] { "public.item" }, UIDocumentPickerMode.Open);
            directoryViewController.AllowsMultipleSelection = true;
            directoryViewController.DidPickDocumentAtUrls += (sender, e) =>
            {
                var r = e.Urls[0].StartAccessingSecurityScopedResource();
                var key = new string[1];
                NSError error;
                var bookmark = e.Urls[0].CreateBookmarkData(NSUrlBookmarkCreationOptions.WithSecurityScope, key, null, out error);
                e.Urls[0].StopAccessingSecurityScopedResource();
                OnFilePicked(bookmark);
            };

            vc.PresentViewController(directoryViewController, true, null);
        }

        protected void OnFilePicked(NSData data)
        {
            var location = data.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
            FilePicked(this, new FileLocationEventArgs(location));
        }

        private NSUrl GetFromString(string data)
        {
            NSData dataToUse = new NSData(data, NSDataBase64DecodingOptions.None);
            NSError error;
            bool isStale;
            var url = NSUrl.FromBookmarkData(dataToUse, NSUrlBookmarkResolutionOptions.WithoutUI, null, out isStale, out error);
            return url;
        }

        public async void SendFileAsync(string path)
        {
            NSFileCoordinator fileCoordinator = new NSFileCoordinator();
            NSError err;

            var pathBookmark = GetFromString(path);
            var serverurl = await SecureStorage.GetAsync("server_url");
            var serverUsername = await SecureStorage.GetAsync("server_username");
            var serverPassword = await SecureStorage.GetAsync("server_password");

            fileCoordinator.CoordinateRead(pathBookmark, NSFileCoordinatorReadingOptions.WithoutChanges, out err, (NSUrl aa) =>
            {
                var pathEnabled = pathBookmark.StartAccessingSecurityScopedResource();

                // Setup Credentials and Server Information
                ConnectionInfo ConnNfo = new ConnectionInfo(serverurl, 22, serverUsername,
                    new AuthenticationMethod[]{

                // Pasword based Authentication
                    new PasswordAuthenticationMethod(serverUsername,serverPassword),
                });

                using (var sftp = new SftpClient(ConnNfo))
                {
                    sftp.Connect();
                    var folderExists = sftp.Exists("/temp/DevTerm");
                    if (!folderExists)
                    {
                        using (var sshClient = new SshClient(ConnNfo))
                        {

                            sshClient.Connect();
                            using (var cmd = sshClient.CreateCommand("mkdir -p ~/temp/DevTerm && chmod +rw ~/temp/DevTerm"))
                            {
                                cmd.Execute();
                                Console.WriteLine("Command>" + cmd.CommandText);
                                Console.WriteLine("Return Value = {0}", cmd.ExitStatus);
                            }
                            sshClient.Disconnect();
                        }
                    }
                    Console.WriteLine(sftp.WorkingDirectory);
                    sftp.ChangeDirectory("./temp/DevTerm");

                    DirectoryInfo di = new DirectoryInfo(pathBookmark.Path);

                    foreach (FileInfo file in di.GetFiles())
                    {
                        using (var uplfileStream = File.OpenRead(file.FullName))
                        {
                            sftp.UploadFile(uplfileStream, file.Name);
                        }
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {

                    }
                    foreach (var file in Directory.EnumerateFileSystemEntries(pathBookmark.Path, "*", SearchOption.AllDirectories))
                    {

                    }

                    sftp.Disconnect();
                }

                pathBookmark.StopAccessingSecurityScopedResource();
            });

        }

        public void ReceiveFiles(string path)
        {

        }
    }
}
